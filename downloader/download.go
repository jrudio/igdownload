// package downloader downloads the image to the filesystem or
// configured storage - S3, Google Drive, etc

package downloader

import (
	"io"
	"net/http"
	"os"
)

// In the future give users options to save to amazon s3, google drive, post to slack, etc
func Download(imgUrl string, path string) (string, error) {
	resp, getErr := http.Get(imgUrl)

	if getErr != nil {
		return "", getErr
	}

	// Set a default path
	if path == "" {
		path = "./img.jpg"
	}

	file, osErr := os.Create(path)

	if osErr != nil {
		return "", osErr
	}

	_, copyErr := io.Copy(file, resp.Body)

	if copyErr != nil {
		return "", copyErr
	}

	file.Close()
	resp.Body.Close()

	// Let caller know where it was written to
	return path, nil
}
