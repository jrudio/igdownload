package downloader_test

import (
	. "github.com/jrudio/igdownload/downloader"
	"testing"
)

func TestDownload(t *testing.T) {
	testUrl := "https://t.co/KKwi4oq6sE"

	_, dlErr := Download(testUrl, "")

	if dlErr != nil {
		t.Errorf("Test Error: %s", dlErr.Error())
	}
}
