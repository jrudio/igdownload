package extractor

import (
	"errors"
	"fmt"
	"github.com/yhat/scrape"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"net/http"
	"regexp"
	"strconv"
)

// GetImageUrl finds and returns the link to an image of an
// instagram post
func GetImageUrl(url string) (string, error) {
	if url == "" {
		return "", errors.New("url required")
	}

	resp, respErr := http.Get(url)

	if respErr != nil {
		return "", respErr
	}

	root, parseErr := html.Parse(resp.Body)

	if parseErr != nil {
		return "", parseErr
	}

	// Get the node that contains "window._sharedData"
	relScriptChunk := regexp.MustCompile(`sharedData`)

	// Extract the link from "display_src"
	displaySrc := regexp.MustCompile(`(display_src)(":")([^"]+)(",")`)

	matcher := func(n *html.Node) bool {
		// Look for script tags
		if n.DataAtom == atom.Script && n.FirstChild != nil {
			// Only match node that contains "_sharedData"
			scriptContent := n.FirstChild.Data

			return relScriptChunk.MatchString(scriptContent)
		}

		return false
	}

	images := scrape.FindAll(root, matcher)

	imagesLen := len(images)

	fmt.Println("Script nodes found: " + strconv.Itoa(imagesLen))

	var link string
	var err error

	for _, image := range images {
		// Grab the text of the found nodes - in this case
		// it should be one script node due to the matcher's
		// tight settings
		// In the future I can probably just parse the script node
		// into json to make it easier to get other relevant information
		txt := scrape.Text(image)

		// From that text, look for "display_src"
		imgLink := displaySrc.FindStringSubmatch(txt)

		// Let the user know it couldn't find it
		if imgLink == nil {
			link = ""
			err = errors.New("Couldn't find display_src")
		}

		// Grab the third group of the matched "display_src" &
		// present user with link
		// fmt.Println("Instagram direct link: \n" + imgLink[3])
		link = imgLink[3]
		err = nil
	}

	// Remove any escaped slashes so Go can interpret the url
	slashRegex := regexp.MustCompile(`\\`)

	link = slashRegex.ReplaceAllString(link, "")

	return link, err
}
