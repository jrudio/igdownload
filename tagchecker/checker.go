package checker

// package main

import (
	"errors"
	// "flag"
	"github.com/headzoo/surf"
	"github.com/headzoo/surf/agent"
	"github.com/headzoo/surf/jar"
	"log"
	"net/url"
	"regexp"
)

func StartChecker(username string, password string) error {
	// func main() {

	// var username, password string

	// flag.StringVar(&username, "username", "", "username for instagram")
	// flag.StringVar(&password, "password", "", "password for instagram")

	// flag.Parse()

	if username == "" {
		return errors.New("username required")
		// log.Fatal("username required")
	}

	if password == "" {
		return errors.New("password required")
		// log.Fatal("password required")
	}

	cj := jar.NewMemoryCookies()

	// Set up browser stuff
	browser := surf.NewBrowser()
	browser.SetCookieJar(cj)
	browser.SetUserAgent(agent.Chrome())

	// Get cookies
	err := browser.Open("https://www.instagram.com/")

	if err != nil {
		return errors.New("Open instagram: " + err.Error())
		// log.Fatal("Open instagram: " + err.Error())
	}

	cookies := browser.SiteCookies()
	csrfTokenRegex := regexp.MustCompile(`(csrftoken)(=)(\w+)`)
	var csrfToken string

	// Extract csrftoken from cookies to POST to login
	for _, cookie := range cookies {
		token := csrfTokenRegex.FindStringSubmatch(cookie.String())

		if len(token) > 0 {
			log.Println("Found token:")

			csrfToken = token[3]
			log.Println(csrfToken)
			break
		}
	}

	if csrfToken == "" {
		return errors.New("Couldn't find csrftoken")
		// log.Fatal("Couldn't find csrftoken")
	}

	// Attempt to login
	loginUrl := "https://www.instagram.com/accounts/login/?force_classic_login"
	err = browser.PostForm(loginUrl, url.Values{
		"username":            {username},
		"password":            {password},
		"csrfmiddlewaretoken": {csrfToken},
	})

	if err != nil {
		return errors.New("Post Err: " + err.Error())
		// log.Fatal("Post Err: " + err.Error())
	}

	if browser.StatusCode() == 302 {
		log.Println("Logged in!")
	} else {
		return errors.New("Failed to login")
		// log.Println("Failed to login")
		// log.Println(browser.StatusCode())
	}

	log.Println(browser.Body())
	return nil
}
