#IGDownloader

###How it's suppose to work

You tag an account (a secondary account that you make) on an Instagram post and it will download that post's image.

###How it currently works
You have to manually pass the IG post url to the program and it will download that image.

##Installation


###Option 1: Download

[Go here to download the latest build](https://github.com/jrudio/igdownload/releases)

####Usage

Either use it directly by `./igdownload -url "<instagram-post-url>"`

or add it to your $PATH

and just do

`igdownload -url "instagram-post-url"`

###Option 2: Install via Go

If you have Go installed

`go get github.com/jrudio/igdownload`

`cd` into that folder

`go install`

###Note

The tag checker - it checks to see if it has been tagged in a post - is not implemented. I need figure how to login to Instagram without using the api. Which might not be possible since Instagram uses React and the scraper I'm using does not load javascript.

Update: I am able to login, but Instagram does not push notifications via their website. So I guess the alternative for the tag checker would be to register as a developer for their api.

The extractor and downloader currently work, though.
