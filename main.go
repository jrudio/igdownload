package main

import (
	"flag"
	"github.com/jrudio/igdownload/downloader"
	"github.com/jrudio/igdownload/extractor"
	"log"
)

var (
	url     string
	imgPath string
)

func main() {
	flag.StringVar(&url, "url", "", "url is the IG post you want to extract")
	flag.StringVar(&imgPath, "path", "", "path is where you want to save the image")
	flag.Parse()

	if url == "" {
		log.Fatal("Please pass a url")
	}

	link, err := extractor.GetImageUrl(url)

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Direct image url:")
	log.Println(link)

	path, dlErr := downloader.Download(link, imgPath)

	if dlErr != nil {
		log.Fatal(dlErr.Error())
	}

	log.Println("Image saved to " + path)
}
